/* 
 * File:   board.h
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:31
 * 
 * On board IO settings and functions for PIC24FJ128GA204.
 */

#ifndef BOARD_H
#define	BOARD_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 *  REGISTER MAPPING
 */

// LEDS
#define LED1_TRIS       TRISAbits.TRISA9
#define LED2_TRIS       TRISAbits.TRISA10
#define LED1            LATAbits.LATA9
#define LED2            LATAbits.LATA10
#define ON              1
#define OFF             0

// BUTTONS
#define BUTTON1_TRIS   TRISCbits.TRISC9
#define BUTTON2_TRIS   TRISCbits.TRISC8
#define BUTTON1        PORTCbits.RC9
#define BUTTON2        PORTCbits.RC8
#define UP              1
#define DOWN            0

// TIMER
#define TIMER_ON        T1CONbits.TON
#define TIMER_PRESCALER T1CONbits.TCKPS
#define TIMER_INTERRUPT IFS0bits.T1IF
#define TIMER_PERIOD    PR1
#define TIMER_CLOCK_SRC T1CONbits.TCS
    
/*
 *  FUNCTIONS
 */
    
void boardInitialize();
void startTimer(int millisec);

#define FCY 4000000UL   // Crystal FREQ

#ifdef	__cplusplus
}
#endif

#endif	/* BOARD_H */

