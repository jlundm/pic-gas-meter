/* 
 * File:   gasmeter.c
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:26
 * 
 * MQ-5 gas sensor settings and functions for PIC24FJ128GA204
 */

#include "gasmeter.h"
#include "xc.h"
#include "libpic30.h" // for __delay_ functions

/*
 * =============================================================================
 * Function: adcInitialize
 * -----------------------
 * Configure the A/D converter.
 * 
 * returns: void
 * =============================================================================
 */
void adcInitialize() {
    GAS_TRIS = 1;               // Set meter bit to input
    GAS_ANS = 1;                // Set meter bit to analog
    AD1CON1bits.ADON = 1;       // Start ADC1
    AD1CON1bits.MODE12 = 0;     // 10-bit result
    AD1CON1bits.SSRC = 0b0111;  // Auto-convert
    AD1CON2bits.PVCFG = 0;      // Positive reference = VDD
    AD1CON2bits.NVCFG0 = 0;     // Negative reference = GND
    AD1CON3bits.SAMC = 10;      // Auto-sample time 10TAD
    AD1CON3bits.ADCS = 10;      // Conversion clock 11 x TCY = TAD
    GAS_CHS_REG = GAS_CHS_BIT;  // Select the right AN bit for AD-register
    AD1CON1bits.FORM = 0b10;    // Output format = "absolute fraction" left just
}

/*
 * =============================================================================
 * Function: adcSample
 * --------------------
 * Performs A/D converter auto sampling.
 * 
 * returns: auto-sampled value from ADC
 * =============================================================================
 */
int adcSample() {
    AD1CON1bits.DONE = 0;       // reset done-bit
    AD1CON1bits.ASAM = 1;       // start auto sample
    while(!AD1CON1bits.DONE);   // wait for done
    AD1CON1bits.ASAM = 0;       // stop auto sample
      
    return(ADC1BUF0);
}

/*
 * =============================================================================
 * Function: adcAvgSample
 * ----------------------
 * Performs a number of A/D converter samplings
 * and calculates the average value (sum of sample values / number of samples).
 * 
 * sampleSize: number of samples to be taken
 * 
 * returns: The average sample value.
 * =============================================================================
 */
double adcAvgSample(int sampleSize) {
    double value;
    
    int i = 0;
    while (i != sampleSize) {
        value = value + adcSample();
        __delay_ms(1);
        i++;
    }
    value = value/sampleSize;
    
    return value;
}

