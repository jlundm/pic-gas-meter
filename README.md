Authors: Johan Lundmark, Robin Meleri.
May 2019

Gas meter program for PIC24FJ128GA204.

To be used with:     DEM 16217 SYH-LY LCD display
                     MQ-5 Gas Sensor

--------------------------------------------------------------------------------

This was made for a school project. First attempt at using a PIC and writing C,
so this might contain lots of bad practices but considering the lack of
examples out there then parts of the program might be of help to someone.

It doesn't really do anything useful, it was mostly made to use all features
that were required for the class.

--------------------------------------------------------------------------------
USER INSTRUCTIONS:
--------------------------------------------------------------------------------

Calibrates on start up and continuously displays CURRENT SAMPLE/BACKGROUND gas
ratio on the LCD screen along with color coding on LED (green=1.00, red=2.00).

- Button RC8: Take longer gas sample
- Button RC9: Return to continuous measurement
- Both buttons: Recalibrate with longer calibration time.


--------------------------------------------------------------------------------
PORT MAPPING:
--------------------------------------------------------------------------------

LCD screen data bits should be connected to first 4 bits of PORTC (make sure
you connect the ones that work with 4 bit mode). RS bit goes to A1, E bit to A0.

Gas meter connects to B3.

All port mappings can be found in respective header file so should be easy to
fit on other PICs as well.


--------------------------------------------------------------------------------
POSSIBLE IMPROVEMENTS:
--------------------------------------------------------------------------------

Many of the LCD display functions should have been consolidated to one
function: one that clears selected row and writes string to that row.

Unfortunately the PIC was returned after project completion so I wont be
updating this.


--------------------------------------------------------------------------------

Examples on using the A/D converter can be found in gasmeter file,
                      PWM can be found in pwm file,
                      Timers can be found in the board file.