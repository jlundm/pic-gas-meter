/* 
 * File:   display.c
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:24
 * 
 * DEM 16217 SYH-LY LCD display settings and functions for PIC24FJ128GA204.
 * Uses 4-bit connection.
 */

#include "display.h"
#include "stdio.h"          // For sprintf function
#include "xc.h"
#include "libpic30.h"       // For __delay_ functions

/*
 * =============================================================================
 * Function: lcdInitialize
 * -----------------------
 * Configure the LCD display.
 * 
 * returns: void
 * =============================================================================
 */
void lcdInitialize() {
   LCD_ANS = 0;                                                                   
   LCD_CFG_TRIS = 0;
   LCD_DATA_TRIS = 0;
   RS_bit = 0;
   E_bit = 0;
    
   __delay_ms(15); 
   lcdPrintInstr(0b11);       // clear display
   lcdPrintInstr(0b10);       // Return home
   lcdPrintInstr(0x28);       // 4 bit interface data, 2 rows, 5x8 font size                                                      
   lcdPrintInstr(0b1100);     // Display on, cursor / cursor position off                                                    
   lcdPrintInstr(0x1);        // clear screen
   lcdPrintInstr(0x6);        // Entry mode
}

/*
 * =============================================================================
 * Function: lcdPrintInstr
 * -----------------------
 * Print 8 bit instruction to the LCD screen
 * through a 4-bit connection.
 * 
 * instr: instruction to be printed.
 * 
 * returns: void
 * =============================================================================
 */
void lcdPrintInstr(unsigned char instr){ 
    
    __delay_ms(1);
    LCD_PORT = ((instr >> 4)& 0x0F);                                                   
    E_EE();
    
    LCD_PORT = (instr & 0x0F);                                                      
    E_EE();
          
}

/*
 * =============================================================================
 * Function: lcdPrintText
 * -----------------------
 * Print a string literal to the LCD screen.
 * 
 * text: String literal to be printed
 * 
 * returns: void
 * =============================================================================
 */
void lcdPrintText(const char *text){                                                   
    RS_bit = 1;                                                                 
    while(*text)                                                                   
        lcdPrintInstr(*text++);                                                        
    
}

/*
 * =============================================================================
 * Function: lcdPrintChar
 * -----------------------
 * Print a character to the LCD screen.
 * 
 * data: Character to be printed
 * 
 * returns: void
 * =============================================================================
 */
void lcdPrintChar(char data){                                                         
    RS_bit = 1;                                                                 
    lcdPrintInstr(data);                                                               
}

/*
 * =============================================================================
 * Function: lcdPrintInt
 * -----------------------
 * Print an integer to the LCD screen.
 * 
 * numb: Integer to be printed
 * 
 * returns: void
 * =============================================================================
 */
void lcdPrintInt(int numb) {
    char numbstring[16];
    sprintf(numbstring, "%d", numb);
    lcdPrintText(numbstring);
}

/*
 * =============================================================================
 * Function: lcdPrintDouble
 * -----------------------
 * Prints a double to the LCD screen with 2 decimals.
 * 
 * numb: Double to be printed
 * 
 * returns: void
 * =============================================================================
 */
void lcdPrintDouble(double numb) {
    char numbstring[16];
    sprintf(numbstring, "%.2f", numb);
    lcdPrintText(numbstring);
}

/*
 * =============================================================================
 * Function: lcdClearScreen
 * -----------------------
 * Clears the LCD screen.
 * 
 * returns: void
 * =============================================================================
 */
void lcdClearScreen() {
    RS_bit = 0;
    lcdPrintInstr(0x1);
}

/*
 * =============================================================================
 * Function: lcdRowOne
 * -----------------------
 * Selects the top row of the LCD screen for printing.
 * 
 * returns: void
 * =============================================================================
 */
void lcdRowOne() {
    RS_bit = 0;
    lcdPrintInstr(0x80);      // DDRAM address select bit = 1, and position 0.
    __delay_ms(1);
}

/*
 * =============================================================================
 * Function: lcdRowTwo
 * -----------------------
 * Selects the bottom row of the LCD screen for printing
 * 
 * returns: void
 * =============================================================================
 */
void lcdRowTwo() {
    RS_bit = 0;
    lcdPrintInstr(0x80 + 40); // DDRAM address select bit = 1, and position 40.
    __delay_ms(1);
}

/*
 * =============================================================================
 * Function: lcdClearRowOne
 * -----------------------
 * Clears the top row of the LCD screen
 * and returns to the beginning of it.
 * 
 * returns: void
 * =============================================================================
 */
void lcdClearRowOne() {
    lcdRowOne();
    RS_bit = 1;
    int count = 0;
    while (count < 16) {    // 16-char width screen
        lcdPrintChar(0x20);
        count++;
    }
    lcdRowOne();
}

/*
 * =============================================================================
 * Function: lcdClearRowTwo
 * -----------------------
 * Clears the bottom row of the LCD screen
 * and returns to the beginning of it.
 * 
 * returns: void
 * =============================================================================
 */
void lcdClearRowTwo() {
    lcdRowTwo();
    RS_bit = 1;
    int count = 0;
    while (count < 16) {    // 16-char width screen
        lcdPrintChar(0x20);
        count++;
    }
    lcdRowTwo();
}