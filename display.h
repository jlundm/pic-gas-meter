/* 
 * File:   display.h
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:24
 * 
 * DEM 16217 SYH-LY LCD display settings and functions for PIC24FJ128GA204.
 * Uses 4-bit connection.
 */

#ifndef DISPLAY_H
#define	DISPLAY_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 *  REGISTER MAPPING
 */
    

#define LCD_PORT        PORTC
#define LCD_ANS         ANSA
#define LCD_CFG_TRIS    TRISA
#define LCD_DATA_TRIS   TRISC
#define RS_bit          PORTAbits.RA1
#define E_bit           PORTAbits.RA0
#define E_EE()          ((E_bit =1), (E_bit = 0))

    
/*
 *  FUNCTIONS
 */
    
void lcdInitialize();
void lcdPrintInstr(unsigned char instr);
void lcdPrintText(const char *text);
void lcdPrintChar(char);
void lcdPrintInt(int numb);
void lcdPrintDouble(double numb);
void lcdClearScreen();
void lcdRowOne();
void lcdRowTwo();
void lcdClearRowOne();
void lcdClearRowTwo();

#define FCY 4000000UL   // Crystal FREQ

#ifdef	__cplusplus
}
#endif

#endif	/* DISPLAY_H */

