/* 
 * File:   board.c
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:31
 * On board IO settings and functions for PIC24FJ128GA204.
 * 
 */

#include "board.h"
#include "xc.h"
#include "libpic30.h"

/* 
 * =============================================================================
 * Function: boardInitialize
 * -----------------------
 * Set up on board IO, IE: the two buttons and their correspoinding LEDs.
 * =============================================================================
 */
void boardInitialize() {
    // BOARD IO: SET AS INPUTS/OUTPUTS
    BUTTON1_TRIS = 1;
    BUTTON2_TRIS = 1;
    LED1_TRIS = 0;
    LED2_TRIS = 0;
    
    // TIMER
    TIMER_PRESCALER = 0b11; // 1:256 timer prescaler
    TIMER_CLOCK_SRC = 0;    // Internal clock (Fosc/2)
}

/* 
 * =============================================================================
 * Function: startTimer
 * -----------------------
 * Sets up the PIC timer period, resets the interrupt flag and starts the timer.
 * Interrupt flag accessible through TIMER_INTERRUPT.
 * 
 * millisec: Timer period value.
 * =============================================================================
 */
void startTimer(int millisec) {
    TIMER_PERIOD = millisec*((FCY/1000)/256); // SET TIMER PERIOD TO DESIRED MS
    TIMER_INTERRUPT = 0;
    TIMER_ON = 0b1;
}