/* 
 * File:   gasmeter.h
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:26
 * 
 * MQ-5 gas sensor settings and functions for PIC24FJ128GA204
 */

#ifndef GASMETER_H
#define	GASMETER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
/*
 *  REGISTER MAPPING
 */

#define GAS_TRIS        TRISBbits.TRISB3
#define GAS_ANS         ANSBbits.ANSB3
#define GAS_CHS_REG     AD1CHSbits.CH0SA    // AN5
#define GAS_CHS_BIT     5                   // AN5

/*
 *  FUNCTIONS
 */

void adcInitialize();
int adcSample();
double adcAvgSample(int sampleSize);


#define FCY 4000000UL   // Crystal FREQ


#ifdef	__cplusplus
}
#endif

#endif	/* GASMETER_H */

