/*
 * File:   mainXC16.c
 * Author: Johan Lundmark, Robin Meleri
 *
 * Created on den 21 maj 2019, 20:18
 * 
 * Gas meter for PIC24FJ128GA204.
 * 
 * To be used with:     DEM 16217 SYH-LY LCD display
 *                      MQ-5 Gas Sensor
 * 
 * Calibrates on start up and continuously displays SAMPLE/BACKGROUND gas ratio
 * 
 * Button RC8: Take longer gas sample
 * Button RC9: Return to continuous measurement
 * Both buttons: Recalibrate with longer calibration time.
 */


#include "mainXC16.h"
#include "xc.h"
#include "libpic30.h"
#include "gasmeter.h"
#include "display.h"
#include "board.h"
#include "pwm.h"



int main(void) {
/* 
 * =============================================================================
 *  PROGRAM START UP
 * =============================================================================
 */
    // VARIABLES
    double backgroundSample;
    int hold;
    
    // INITIALIZE COMPONENTS
    lcdInitialize();
    adcInitialize();
    boardInitialize();
    pwmInitialize();
    
    // START-UP CALIBRATION
    backgroundSample = calibrate(STARTUP_CALIBRATION_SAMPLE_SIZE);
    
/* 
 * =============================================================================
 *  MAIN LOOP
 * =============================================================================
 */
    while (1) {
        // SHORT "CONTINUOUS" MEASUREMENT
        continuousMeasurement(backgroundSample);
        
        // USER FUNCTION SELECTION
        hold = 0;
        while (hold < USER_INPUT_TIME) {
            hold++;
            
            // RE-CALIBRATE (WITH LONGER SAMPLING)
            if (BUTTON1 == DOWN && BUTTON2 == DOWN) {
                __delay_ms(DEBOUNCE_TIME);
                if (BUTTON1 == DOWN && BUTTON2 == DOWN) {
                    backgroundSample = calibrate(RECALIBRATION_SAMPLE_SIZE);
                }
            }
            
            // LONG MEASURE
            if (BUTTON2 == DOWN) {
                __delay_ms(DEBOUNCE_TIME);
                if(BUTTON2 == DOWN) {
                    longMeasurement(backgroundSample);
                }   
            }
        }
        // CLEAR WATCHDOG TIMER
        ClrWdt();
    }
    return 0;
}

/* 
 * =============================================================================
 * Function: calibrate
 * -----------------------
 * Gets an average sample value where sampleSize = n samples,
 * to be used as background gas comparison. Displays CALIBRATION_ messages.
 * 
 * sampleSize: n samples to be collected
 * 
 * returns: average value
 * =============================================================================
 */
double calibrate(int sampleSize) {
    // INDICATOR LIGHTS
    LED1 = ON;
    LED2 = ON;
    
    // VAR
    double backgroundSample;
    
    // COLLECT SAMPLE & PRINT MESSAGES
    lcdClearScreen();
    lcdPrintText(CALIBRATION_MESSAGE_1);
    backgroundSample = adcAvgSample(sampleSize);
    lcdClearScreen();
    lcdPrintText(CALIBRATION_MESSAGE_2);
    lcdRowTwo();
    lcdPrintDouble(backgroundSample);
    __delay_ms(CALIBRATION_MESSAGE_TIME);
    lcdClearScreen();
    
    // RETURN SAMPLE
    return backgroundSample;
}

/*
 * =============================================================================
 * Function: continuousMeasurement
 * -----------------------
 * Gets an average sample value where CONTINUOUS_SAMPLE_SIZE = n samples,
 * and displays the ratio of current sample/background gas along
 * with CONTINUOUS_ messages on the LCD.
 * 
 * Blends GREEN/RED LEDs according to ratio.
 * 
 * backgroundSample: used to calculate the sample / background gas ratio.
 * 
 * returns: void
 * =============================================================================
 */
void continuousMeasurement(double backgroundSample) {
    // INDICATOR LIGHTS
    LED1 = ON;
    LED2 = OFF;
    
    // SAMPLE, CALCULATE AND DISPLAY RATIO ON LCD
    double sample = adcAvgSample(CONTINUOUS_SAMPLE_SIZE);
    double ratio = sample/backgroundSample;
    lcdRowOne();
    lcdPrintText(CONTINUOUS_SAMPLE_MESSAGE);
    lcdRowTwo();
    lcdPrintDouble(ratio);
    
    // BLEND LEDs
    pwmBlendLeds(ratio);
}

/*
 * =============================================================================
 * Function: longMeasurement
 * -----------------------
 * Gets an average sample value where LONG_SAMPLE_SIZE = n samples,
 * and displays the ratio of current sample/background gas along
 * with LONG_SAMPLE_ messages on the LCD. Then holds the program while
 * displaying, until user presses any on board button.
 * 
 * Blends green/red LEDs according to ratio.
 * 
 * backgroundSample: used to calculate the sample / background gas ratio.
 * 
 * returns: void
 * =============================================================================
 */
void longMeasurement(double backgroundSample) {
    // INDICATOR LIGHTS
    LED1 = OFF;
    LED2 = ON;
    
    // VAR
    double ratio;
    int counter;
    int hold;
    
    // SAMPLE, CALCULATE AND DISPLAY RATIO ON LCD BOTTOM ROW
    lcdClearScreen();
    lcdRowOne();
    lcdPrintText(LONG_SAMPLE_MESSAGE_1);
    ratio = adcAvgSample(LONG_SAMPLE_SIZE)/backgroundSample;
    lcdClearScreen();
    lcdRowTwo();
    lcdPrintDouble(ratio);
    
    // BLEND LEDs
    pwmBlendLeds(ratio);
                    
    // HOLD VALUE WITH ALTERNATING TOP ROW MESSAGE UNTIL KEY PRESS
    counter = 0;
    TIMER_INTERRUPT = 1;
                    
    while (hold != 0) {
        
        if (TIMER_INTERRUPT == 1) {
            // DISPLAY ON LCD TOP ROW
            lcdClearRowOne();
            if (counter%2 == 0) {
                lcdPrintText(LONG_SAMPLE_MESSAGE_2);
                startTimer(LONG_SAMPLE_MSG_ALT_FREQ);
            }   else {
                lcdPrintText(LONG_SAMPLE_MESSAGE_3);
                startTimer(LONG_SAMPLE_MSG_ALT_FREQ/2);
            }
                            
            // FLIP COUNTER
            counter = counter^0b1;
        }
                        
        // CHECK INPUT
        if (BUTTON1 == DOWN || BUTTON2 == DOWN) {
            __delay_ms(DEBOUNCE_TIME);
            if (BUTTON1 == DOWN || BUTTON2 == DOWN) {
                hold = 0;   // BREAK LOOP
            }
        }
    }
}
