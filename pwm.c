/* 
 * File:   pwm.c
 * Author: johanlundmark
 *
 * Created on May 22, 2019, 3:51 PM
 * 
 * Pulse Width Modulation settings and functions for PIC24FJ128GA204 gas meter
 */

#include "pwm.h"
#include "xc.h"


/*
 * =============================================================================
 * Function: pwmInitialize
 * -----------------------
 * Assigns (mostly) hard-coded values to OCx bits of three OCx,
 * since it would not perform as PWM with other values.
 * =============================================================================
 */
void pwmInitialize() {
    /*
     * RED LED
     */
    PWM_RED_RP = PWM_RED_OC;    // Assign LED RP to output function (OCx)
    
    PWM_RED_SYNCSEL = 0x1F;     // sync = this OC module
    PWM_RED_OCTRIG = 0;         // Use SYNCSEL as synchronize not trigger
    PWM_RED_OCTSEL = 0b111;     //Output compare timer select: Fcy
    PWM_RED_OCM = 0b110;        //Output compare align: 110 = edge, 111 = center 
    PWM_RED_TRIGSTAT = 1;
    
    PWM_RED_PERIOD = PWM_FREQ;  // PERIOD (0x03FF = 3.9khz)
    PWM_RED_DUTY_CYCLE = 0;     // Duty cycle 10 bit res (all on = 1*period,
                                // all off = 0*period)
    
    
    /*
     * GREEN LED
     */
    PWM_GREEN_RP = PWM_GREEN_OC;// Assign LED RP to output function (OCx)
    
    PWM_GREEN_SYNCSEL = 0x1F;   // sync = this OC module
    PWM_GREEN_OCTRIG = 0;       // Use SYNCSEL as synchronize not trigger
    PWM_GREEN_OCTSEL = 0b111;   //Output compare timer select: Fcy
    PWM_GREEN_OCM = 0b110;      //Output compare align: 110 = edge, 111 = center 
    PWM_GREEN_TRIGSTAT = 1;
    
    PWM_GREEN_PERIOD = PWM_FREQ;// PERIOD (0x03FF = 3.9khz)
    PWM_GREEN_DUTY_CYCLE = 0;   // Duty cycle 10 bit res (all on = 1*period,
                                // all off = 0*period)
    
    
    /*
     * BLUE LED
     */
    PWM_BLUE_RP = PWM_BLUE_OC;  // Assign LED RP to output function (OCx)
    
    PWM_BLUE_SYNCSEL = 0x1F;    // sync = this OC module
    PWM_BLUE_OCTRIG = 0;        // Use SYNCSEL as synchronize not trigger
    PWM_BLUE_OCTSEL = 0b111;    //Output compare timer select: Fcy
    PWM_BLUE_OCM = 0b110;       //Output compare align: 110 = edge, 111 = center 
    PWM_BLUE_TRIGSTAT = 1;
    
    PWM_BLUE_PERIOD = PWM_FREQ; // PERIOD (0x03FF = 3.9khz)
    PWM_BLUE_DUTY_CYCLE = 0;    // Duty cycle 10 bit res (all on = 1*period,
                                // all off = 0*period)
    
    
}


/*
 * =============================================================================
 * Function: pwmBlendLeds
 * -----------------------
 * Blends the LEDS from green to red based on gas ratio
 * where green = PWM_GOOD_RATIO, red = PWM_BAD_RATIO.
 * If below green or above red, respective is capped at PWM_MAX_VALUE
 * else the LED duty cycles follow a simple kx+m linear
 * equation where, for green LED,
 * k = (0 - max light)/(bad ratio - good ratio)
 * m = max light - (k*good ratio)
 * x = ratio
 * 
 * and for red LED kx+m:
 * k = (max light - 0) / (bad ratio - good ratio)
 * m = 0 - (k*good ratio)
 * x = ratio
 * 
 * ratio = gas ratio to be indicated by LEDs
 * =============================================================================
 */
void pwmBlendLeds(double ratio) {
    /*
     * GREEN LED
     */
    double k = (0 - PWM_MAX_VALUE) / (PWM_BAD_RATIO - PWM_GOOD_RATIO);
    double m = PWM_MAX_VALUE - (k*PWM_GOOD_RATIO);
    
    if (ratio <= PWM_GOOD_RATIO)
        PWM_GREEN_DUTY_CYCLE = PWM_MAX_VALUE;
    else if (ratio <= PWM_BAD_RATIO)
        PWM_GREEN_DUTY_CYCLE = (int) (k*ratio) + m;
    else
        PWM_GREEN_DUTY_CYCLE = 0;
    
    
    /*
     * RED LED
     */
    
    k = (PWM_MAX_VALUE) / (PWM_BAD_RATIO - PWM_GOOD_RATIO);
    m = -(k*PWM_GOOD_RATIO);
    
    if (ratio <= PWM_GOOD_RATIO)
        PWM_RED_DUTY_CYCLE = 0;
    else if (ratio <= PWM_BAD_RATIO)
        PWM_RED_DUTY_CYCLE = (int) (k*ratio) + m;
    else
        PWM_RED_DUTY_CYCLE = PWM_MAX_VALUE;
    
}