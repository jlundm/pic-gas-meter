/* 
 * File:   pwm.h
 * Author: johanlundmark
 *
 * Created on May 22, 2019, 3:52 PM
 * 
 * Pulse Width Modulation settings and functions for PIC24FJ128GA204 gas meter
 */

#ifndef PWM_H
#define	PWM_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * SETTINGS
 */
#define PWM_GOOD_RATIO 1
#define PWM_BAD_RATIO 2

    
#define PWM_MAX_VALUE 0b111111
#define PWM_FREQ 0x03FF
    
    
/*
 * REGISTER MAPPING
 */

#define PWM_RED_DUTY_CYCLE      OC1R
#define PWM_RED_PERIOD          OC1RS
#define PWM_GREEN_DUTY_CYCLE    OC2R
#define PWM_GREEN_PERIOD        OC2RS   
#define PWM_BLUE_DUTY_CYCLE     OC3R
#define PWM_BLUE_PERIOD         OC3RS    
    
#define PWM_RED_RP          _RP21R
#define PWM_RED_OC          13
#define PWM_RED_SYNCSEL     OC1CON2bits.SYNCSEL
#define PWM_RED_OCTRIG      OC1CON2bits.OCTRIG
#define PWM_RED_OCTSEL      OC1CON1bits.OCTSEL
#define PWM_RED_OCM         OC1CON1bits.OCM
#define PWM_RED_TRIGSTAT    OC1CON2bits.TRIGSTAT

#define PWM_GREEN_RP        _RP22R
#define PWM_GREEN_OC        14
#define PWM_GREEN_SYNCSEL   OC2CON2bits.SYNCSEL
#define PWM_GREEN_OCTRIG    OC2CON2bits.OCTRIG
#define PWM_GREEN_OCTSEL    OC2CON1bits.OCTSEL
#define PWM_GREEN_OCM       OC2CON1bits.OCM
#define PWM_GREEN_TRIGSTAT  OC2CON2bits.TRIGSTAT

#define PWM_BLUE_RP         _RP23R
#define PWM_BLUE_OC         15    
#define PWM_BLUE_SYNCSEL    OC3CON2bits.SYNCSEL
#define PWM_BLUE_OCTRIG     OC3CON2bits.OCTRIG
#define PWM_BLUE_OCTSEL     OC3CON1bits.OCTSEL
#define PWM_BLUE_OCM        OC3CON1bits.OCM
#define PWM_BLUE_TRIGSTAT   OC3CON2bits.TRIGSTAT   
    
/*
 * FUNCTIONS
 */    
void pwmInitialize();
void pwmBlendLeds(double ratio);

#ifdef	__cplusplus
}
#endif

#endif	/* PWM_H */

